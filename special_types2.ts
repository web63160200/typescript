let w:unknown=1;
w="string";
w={
    runANonExistentMethod: () =>{ // function
        console.log("I think therefore I am");

    }
}as { runANonExistentMethod:() => void}

if(typeof w ==='object' && w!== null){
    (w as {runANonExistentMethod: Function}).runANonExistentMethod ();
}